//@ts-check

const { buildSchema } = require("graphql")
const { graphqlHTTP } = require("express-graphql")

const HandlerTalent = require("../../../handlers/talent")
const { SchemaGraphQL } = require("./schema")

/**
 * @param {HandlerTalent} handlerTalent
 * @returns {(request: import("express").Request, response: import("express").Response) => Promise<void>}
 */
function BuildGraphQLRouterTalent(handlerTalent){    
  
  return graphqlHTTP({
    schema: buildSchema(SchemaGraphQL),    
    rootValue: {      
      login: handlerTalent.queryLogin.bind(handlerTalent),
      getInfos: handlerTalent.queryGetInfos.bind(handlerTalent),
      createTalent: handlerTalent.mutationCreateTalent.bind(handlerTalent),
      addCompany: handlerTalent.mutationAddCompany.bind(handlerTalent),
      deleteCompany: handlerTalent.mutationDeleteCompany.bind(handlerTalent),
    },
    graphiql: true
  })

}

module.exports = BuildGraphQLRouterTalent