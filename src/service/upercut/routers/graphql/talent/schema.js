//@ts-check

// const { TypeCompany } = require("../company/schema")

const TypeCompany = `
  type Company {
    _id: ID!
    name: String!
    password: String!
    yearOfCreation: Int!
    user: [Talent]
  }
`

const InputTypeTalent = `
  input TalentInput {    
    name: String!
    email: String!
    password: String!
  }
`

const TypeTalent = `
  type Talent {
    _id: ID!
    name: String!
    email: String!
    password: String!    
    company: Company
  }
`

const TypeAuthTalent = `
  type AuthTalent {
    email: String!
    token: String!
  }
`

const RootQuery = `
  type RootQuery {
    getInfos: Talent
    login(email: String!, password: String!): AuthTalent
  }
`

const RootMutation = `
  type RootMutation {
    createTalent(talentInput: TalentInput): Boolean
    addCompany(id: ID!): Boolean
    deleteCompany: Boolean    
  }
`

const SchemaGraphQL = `

  ${TypeCompany}

  ${InputTypeTalent}

  ${TypeTalent}

  ${TypeAuthTalent}

  ${RootQuery}

  ${RootMutation}

  schema {
    query: RootQuery
    mutation: RootMutation    
  }
`

module.exports = {
  SchemaGraphQL,
  TypeTalent
}