//@ts-check

const { buildSchema } = require("graphql")
const { graphqlHTTP } = require("express-graphql")


const { SchemaGraphQL } = require("./schema")
const HandlerCompany = require("../../../handlers/company")

/**
 * @param {HandlerCompany} handlerCompany
 * @returns {(request: import("express").Request, response: import("express").Response) => Promise<void>}
 */
function BuildGraphQLRouterCompany(handlerCompany){    
  
  return graphqlHTTP({
    schema: buildSchema(SchemaGraphQL),    
    rootValue: {   
      login: handlerCompany.queryLogin.bind(handlerCompany),   
      getInfos: handlerCompany.queryGetInfos.bind(handlerCompany),
      createCompany: handlerCompany.mutationCreateCompany.bind(handlerCompany),
      addTalent: handlerCompany.mutationAddTalent.bind(handlerCompany),
      deleteTalent: handlerCompany.mutationDeleteTalent.bind(handlerCompany),
      deleteTalents: handlerCompany.mutationDeleteTalents.bind(handlerCompany),
    },
    graphiql: true
  })

}

module.exports = BuildGraphQLRouterCompany