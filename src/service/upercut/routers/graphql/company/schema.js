//@ts-check

// const { TypeTalent } = require("../talent/schema")

const TypeTalent = `
  type Talent {
    _id: ID!
    name: String!
    email: String!
    password: String!    
    company: Company
  }
`

const InputTypeCompany = `
  input CompanyInput {    
    name: String!
    password: String!
    yearOfCreation: Int!
  }
`

const TypeCompany = `
  type Company {
    _id: ID!
    name: String!
    password: String!
    yearOfCreation: Int!
    user: [Talent]
  }
`

const TypeAuthCompany = `
  type AuthCompany {
    name: String!
    token: String!
  }
`

const RootQuery = `
  type RootQuery {
    getInfos: Company    
    login(name: String!, password: String!): AuthCompany
  }
`

const RootMutation = `
  type RootMutation {
    createCompany(companyInput: CompanyInput): Boolean
    addTalent(id: ID!): Boolean
    deleteTalent(id: ID!): Boolean
    deleteTalents: Boolean
  }
`

const SchemaGraphQL = `

  ${TypeTalent}

  ${InputTypeCompany}

  ${TypeCompany}

  ${TypeAuthCompany}

  ${RootQuery}

  ${RootMutation}

  schema {
    query: RootQuery
    mutation: RootMutation    
  }
`

module.exports = {
  TypeCompany,
  SchemaGraphQL
}