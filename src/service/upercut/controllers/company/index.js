//@ts-check

const { Model, Types } = require("mongoose")
const ObjectId = Types.ObjectId

const PkgJWT = require("../../../../pkg/pkg_jwt")

class ControllerCompany {

  /** @type {Model} */
  #modelCompany

  /** @type {Model} */
  #modelTalent

  /** @type {PkgJWT} */
  #pkgJWT

  /** 
   * @constructor 
   * @param {Model} modelCompany 
   * @param {Model} modelTalent 
   * @param {PkgJWT} pkgJWT 
   */
  constructor(modelCompany, modelTalent, pkgJWT){
    this.#modelCompany = modelCompany
    this.#modelTalent = modelTalent
    this.#pkgJWT = pkgJWT    
  }

  /**
   * @typedef {object} LoginForCompanyData
   * @property {string} name
   * @property {string} token
   */

  /**
   * @typedef {object} LoginForCompanyResponse
   * @property {LoginForCompanyData} data
   * @property {any} error
  */

  /**   
   * @param {string} name 
   * @param {string} password 
   * @returns {Promise<LoginForCompanyResponse>}
  */
  async loginForCompany(name, password){

    let companyDetail = await this.#modelCompany.findOne({name})
    // @ts-ignore
    if(Object.keys(companyDetail) === 0){
      return {
        data: {
          name,
          token: ""
        },
        error: new Error("user with that name not exists")
      }
    }

    if(companyDetail.password != password){
      return {
        data: {
          name,
          token: ""
        },
        error: new Error("password mismatch")
      }
    }    

    // @ts-ignore
    const generateTokenResponse = this.#pkgJWT.generateToken({
      data: companyDetail,
      tipe:"company"
    })
    console.log(generateTokenResponse.error)
    if(generateTokenResponse.error != null){
      return {
        data: {
          name,
          token: ""
        },
        error: generateTokenResponse.error
      }
    }
    
    return {
      data: {
        name, 
        token: generateTokenResponse.token
      },
      error: null
    }

  }

  /**
   * @typedef {object} CreateNewCompanyRequest
   * @property {string} name
   * @property {string} password
   * @property {number} yearOfCreation
  */

  /**
   * @typedef {object} CreateNewCompanyResponse   
   * @property {any} error
  */

  /**
   * @param {CreateNewCompanyRequest} args
   * @returns {Promise<CreateNewCompanyResponse>}
  */
  async createNewCompany(args){

    const companyIsExists = await this.#modelCompany.exists({name:args.name})
    if(companyIsExists){
      return {
        error: new Error("company already exist")
      }
    }
    
    const company = new this.#modelCompany({
      name: args.name,
      password: args.password,
      yearOfCreation: args.yearOfCreation
    })

    try {
      await company.save()                
    } catch (error) {
      return {
        error
      }
    }
    
    return {
      error: null
    }

  }

  /**
   * @typedef {object} AddNewTalentResponse   
   * @property {any} error
  */

  /**   
   * @param {string} idTalent 
   * @param {string} idCompany 
   * @returns {Promise<AddNewTalentResponse>}
  */
  async addNewTalent(idTalent, idCompany){
    

    const company = await this.#modelCompany.findById(idCompany)    
    company.user.push(idTalent)    

    
    try {
      await this.#modelTalent.updateOne(
        {
          _id: new ObjectId(idTalent),      
        },
        {
          company: new ObjectId(company._id)
        }
      )
      await company.save()                
    } catch (error) {
      return {
        error
      }
    }

    return {
      error: null
    }

  }

  /**
   * @typedef {object} DeleteOneTalentResponse   
   * @property {any} error
  */

  /**   
   * @param {string} idTalent 
   * @param {string} idCompany 
   * @returns {Promise<DeleteOneTalentResponse>}
  */
  async deleteOneTalent(idTalent, idCompany){

    const company = await this.#modelCompany.findById(idCompany)    

    company.user = company.user.filter((id)=>{
      return (id != idTalent)      
    })       

    try {
      await this.#modelTalent.updateOne(
        {
          _id: new ObjectId(idTalent),      
        },
        {
          company: null
        }
      )
      await company.save()                
    } catch (error) {
      return {
        error
      }
    }

    return {
      error: null
    }

  }


  /**
   * @typedef {object} DeleteAllTalentResponse   
   * @property {any} error
  */

  /**      
   * @param {string} idCompany 
   * @returns {Promise<DeleteAllTalentResponse>}
  */
  async deleteAllTalent(idCompany){

    const company = await this.#modelCompany.findById(idCompany)        

    try {
      for(const item of company.user){
        await this.#modelTalent.updateOne(
          {
            _id: new ObjectId(item),      
          },
          {
            company: null
          }
        )
      }
      company.user = []
      await company.save()                
    } catch (error) {
      return {
        error
      }
    }

    return {
      error: null
    }

  }

}

module.exports = ControllerCompany