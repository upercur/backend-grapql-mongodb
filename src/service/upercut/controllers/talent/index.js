const { Model, Types } = require("mongoose")
const ObjectId = Types.ObjectId
const PkgJWT = require("../../../../pkg/pkg_jwt")

class ControllerTalent {

  /** @type {Model} */
  #modelCompany

  /** @type {Model} */
  #modelTalent

  /** @type {PkgJWT} */
  #pkgJWT

  /** 
   * @constructor 
   * @param {Model} modelCompany 
   * @param {Model} modelTalent 
   * @param {PkgJWT} pkgJWT 
   */
  constructor(modelCompany, modelTalent, pkgJWT){
    this.#modelCompany = modelCompany
    this.#modelTalent = modelTalent
    this.#pkgJWT = pkgJWT    
  }

  /**
   * @typedef {object} LoginForTalentData
   * @property {string} email
   * @property {string} token
   */

  /**
   * @typedef {object} LoginForTalentResponse
   * @property {LoginForTalentData} data
   * @property {any} error
  */

  /**   
   * @param {string} email 
   * @param {string} password 
   * @returns {Promise<LoginForTalentResponse>}
  */
  async loginForTalent(email, password){

    let talentDetail = await this.#modelTalent.findOne({email})
    if(Object.keys(talentDetail) === 0){
      return {
        data: {
          email,
          token: ""
        },
        error: new Error("user with that name not exists")
      }
    }

    if(talentDetail.password != password){
      return {
        data: {
          email,
          token: ""
        },
        error: new Error("password mismatch")
      }
    }        

    const generateTokenResponse = this.#pkgJWT.generateToken({
      data: talentDetail,
      tipe: "talent"
    })    
    if(generateTokenResponse.error != null){
      return {
        data: {
          email,
          token: ""
        },
        error: generateTokenResponse.error
      }
    }
    
    return {
      data: {
        email, 
        token: generateTokenResponse.token
      },
      error: null
    }

  }

  /**
   * @typedef {object} Talent
   * @property {string} _id
   * @property {string} name
   * @property {string} password
   * @property {string} email
   * @property {any} company
  */

  /**   
   * @param {string} id 
   * @returns {Promise<Talent>}
   */

  async getDetailTalent(id){

    const talent = await this.#modelTalent.find({
      _id: new ObjectId(id)
    }).populate("company")

    return talent[0]

  }

  /**
   * @typedef {object} CreateNewTalentRequest
   * @property {string} name
   * @property {string} password
   * @property {string} email
  */

  /**
   * @typedef {object} CreateNewTalentResponse   
   * @property {any} error
  */

  /**
   * @param {CreateNewTalentRequest} args
   * @returns {Promise<CreateNewTalentResponse>}
  */
  async createNewTalent(args){

    const talentIsExists = await this.#modelTalent.exists({email:args.email})
    if(talentIsExists){
      return {
        error: new Error("talent already exist")
      }
    }
    
    const talent = new this.#modelTalent({
      name: args.name,
      password: args.password,
      email: args.email
    })

    try {
      await talent.save()                
    } catch (error) {
      return {
        error
      }
    }
    
    return {
      error: null
    }

  }

  /**
   * @typedef {object} AddNewCompanyResponse   
   * @property {any} error
  */

  /**   
   * @param {string} idTalent 
   * @param {string} idCompany 
   * @returns {Promise<AddNewTalentResponse>}
  */
  async addNewCompany(idTalent, idCompany){

    const talent = await this.#modelTalent.findById(idTalent)
    const company = await this.#modelCompany.findById(idCompany)

    company.user.push(idTalent)
    talent.company = idCompany

    try {
      await talent.save()
      await company.save()                
    } catch (error) {
      return {
        error
      }
    }

    return {
      error: null
    }

  }

  /**
   * @typedef {object} DeleteCompanyResponse   
   * @property {any} error
  */

  /**   
   * @param {string} idTalent    
   * @returns {Promise<DeleteCompanyResponse>}
  */
  async deleteCompany(idTalent){

    const talent = await this.#modelTalent.findById(idTalent)
    const company = await this.#modelCompany.findById(talent.company)

    company.user = company.user.filter((id)=>{
      return (id != idTalent)      
    })       
    talent.company = null

    try {
      await talent.save()
      await company.save()                
    } catch (error) {
      return {
        error
      }
    }

    return {
      error: null
    }

  }

}

module.exports = ControllerTalent