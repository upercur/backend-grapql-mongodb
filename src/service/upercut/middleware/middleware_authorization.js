const PkgJWT = require("../../../pkg/pkg_jwt");

/**
 * @param {PkgJWT} pkgJWT
 * @returns {(request: import("express").Request, response: import("express").Response, next: import("express").NextFunction)}
*/
function GenerateAuthorization(pkgJWT){
  return (request, response, next) => {
    
    const token = request.headers.authorization    

    console.log(request.url)


    if(typeof token === "undefined" || token === ""){
      console.log("masuk")
      request["isAuth"] = false
      next();
      return
    }
    
    const verificationResponse = pkgJWT.verification(token)    
    if(verificationResponse.error != null){
      request["isAuth"] = false
      next();
      return
    }    

    console.log(verificationResponse.data.claim["tipe"], "tipe")
    console.log(request.url.split("/")[4], "url")

    if(verificationResponse.data.claim["tipe"] != request.url.split("/")[4]){
      request["isAuth"] = false
      next();
      return
    }
    
    request["isAuth"] = true
    request["dataAuth"] = verificationResponse.data
    next();

  } 
}

module.exports = GenerateAuthorization