const mongoose = require('mongoose');

const schema = mongoose.Schema

const companySchema = new schema({
  name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true
  },
  yearOfCreation: {
    type: Number,
    required: true
  },
  user: [
    {
      type: schema.Types.ObjectId,
      ref: "Talent"
    }
  ]
});

module.exports = mongoose.model("Company", companySchema)