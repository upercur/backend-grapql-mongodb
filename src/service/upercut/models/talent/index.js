const mongoose = require('mongoose');

const schema = mongoose.Schema

const talentSchema = new schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true
  },
  company:{
    type: schema.Types.ObjectId,
    ref: "Company"
  }

});

module.exports = mongoose.model("Talent", talentSchema)