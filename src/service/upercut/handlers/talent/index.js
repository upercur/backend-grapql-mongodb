//@ts-check

const ControllerTalent = require("../../controllers/talent")

class HandlerTalent {  

  /** @type {ControllerTalent} */
  #controllerTalent

  /**
   * @constructor
   * @param {ControllerTalent} controllerTalent
  */
  constructor(controllerTalent) {
    this.#controllerTalent = controllerTalent
  }

  /**
   * @typedef {object} Talent
   * @property {string} _id
   * @property {string} name
   * @property {string} password
   * @property {string} email
   * @property {any} company
  */

  /**
   * @param {import("express").Request} request
   * @returns {Promise<Talent>}
  */
  async queryGetInfos(args, request) {

    if(!request["isAuth"]){
      throw new Error("Authorization")      
    }

    const dataAuth = request["dataAuth"]["claim"]["data"]        

    const talentDetail = await this.#controllerTalent.getDetailTalent(dataAuth._id)

    console.log(talentDetail)

    return {
      _id:dataAuth._id,
      name:dataAuth.name,
      password:dataAuth.password,
      email:dataAuth.email,
      company: talentDetail.company
    }
  }

  /**
   * @typedef {object} AuthTalent
   * @property {string} email
   * @property {string} token
   */

  /**   
   * @param {{email:string, password:string}} args   
   * @returns {Promise<AuthTalent>}
   */
  async queryLogin({email, password}){


    const controllerTalentResponse = await this.#controllerTalent.loginForTalent(
      email,
      password
    )

    if(controllerTalentResponse.error != null) {
      throw new Error(controllerTalentResponse.error)      
    }

    return {
      email,
      token: controllerTalentResponse.data.token
    }

  }


  /**
   * @typedef {object} TalentInput
   * @property {string} name
   * @property {string} email
   * @property {string} password
  */

  /**   
   * @param {{talentInput:TalentInput}} args 
   * @param {import("express").Request} request
   * @returns {Promise<boolean>}
  */
  async mutationCreateTalent({talentInput}, request) {
    
    const controllerTalentResponse = await this.#controllerTalent.createNewTalent({
      name: talentInput.name,
      password: talentInput.password,
      email: talentInput.email
    })

    if(controllerTalentResponse.error != null){
      return false
    }

    return true
  }

  /**   
   * @param {{id:string}} args 
   * @param {import("express").Request} request
   * @returns {Promise<boolean>}
  */
  async mutationAddCompany({id}, request) {

    if(!request["isAuth"]){
      throw new Error("Authorization")      
    }

    const dataAuth = request["dataAuth"]["claim"]["data"]    

    const controllerTalentResponse = await this.#controllerTalent.addNewCompany(
      dataAuth._id,
      id,
    )
    if(controllerTalentResponse.error != null){      
      return false
    }

    return true
  }

  /**      
   * @param {import("express").Request} request
   * @returns {Promise<boolean>}
  */
  async mutationDeleteCompany(args, request) {

    if(!request["isAuth"]){
      throw new Error("Authorization")      
    }

    const dataAuth = request["dataAuth"]["claim"]["data"]    

    const controllerTalentResponse = await this.#controllerTalent.deleteCompany(
      dataAuth._id      
    )
    if(controllerTalentResponse.error != null){      
      return false
    }

    return true
  }
  

}

module.exports = HandlerTalent