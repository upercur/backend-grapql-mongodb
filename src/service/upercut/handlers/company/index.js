//@ts-check

const ControllerCompany = require("../../controllers/company")

class HandlerCompany {

  /** @type {ControllerCompany} */
  #controllerCompany

  /**   
   * @constructor
   * @param {ControllerCompany} controllerCompany 
   */
  constructor(controllerCompany){
    this.#controllerCompany = controllerCompany
  }

    /**
   * @typedef {object} Company
   * @property {string} _id
   * @property {string} name
   * @property {string} password
   * @property {number} yearOfCreation
  */  

  /**
   * @param {import("express").Request} request
   * @returns {Company}
  */
  queryGetInfos(args, request) {

    if(!request["isAuth"]){
      throw new Error("Authorization")      
    }

    const dataAuth = request["dataAuth"]["claim"]["data"]

    return {
      _id:dataAuth._id,
      name:dataAuth.name,
      password:dataAuth.password,
      yearOfCreation:dataAuth.yearOfCreation0
    }
  }

  /**
   * @typedef {object} AuthCompany
   * @property {string} name
   * @property {string} token
   */

  /**   
   * @param {{name:string, password:string}} args   
   * @returns {Promise<AuthCompany>}
   */
  async queryLogin({name, password}){    

    const controllerCompanyResponse = await this.#controllerCompany.loginForCompany(
      name,
      password
    )

    if(controllerCompanyResponse.error != null) {
      throw new Error(controllerCompanyResponse.error)      
    }

    return {
      name,
      token: controllerCompanyResponse.data.token
    }

  }


  /**
   * @typedef {object} CompanyInput
   * @property {string} name
   * @property {string} password
   * @property {number} yearOfCreation
  */

  /**   
   * @param {{companyInput:CompanyInput}} args 
   * @param {import("express").Request} request
   * @returns {Promise<boolean>}
  */
  async mutationCreateCompany({companyInput}, request) {         
    
    const controllerCompanyResponse = await this.#controllerCompany.createNewCompany({
      name: companyInput.name,
      password: companyInput.password,
      yearOfCreation: companyInput.yearOfCreation
    })

    if(controllerCompanyResponse.error != null){
      return false
    }

    return true
  }

  /**   
   * @param {{id:string}} args 
   * @param {import("express").Request} request
   * @returns {Promise<boolean>}
  */
  async mutationAddTalent({id}, request) {

    if(!request["isAuth"]){
      throw new Error("Authorization")      
    }

    const dataAuth = request["dataAuth"]["claim"]["data"]

    const controllerCompanyResponse = await this.#controllerCompany.addNewTalent(
      id,
      dataAuth._id
    )
    if(controllerCompanyResponse.error != null){      
      return false
    }

    return true
  }

  /**   
   * @param {{id:string}} args 
   * @param {import("express").Request} request
   * @returns {Promise<boolean>}
  */
  async mutationDeleteTalent({id}, request) {

    if(!request["isAuth"]){
      throw new Error("Authorization")      
    }

    const dataAuth = request["dataAuth"]["claim"]["data"]

    const controllerCompanyResponse = await this.#controllerCompany.deleteOneTalent(
      id,
      dataAuth._id
    )
    if(controllerCompanyResponse.error != null){      
      return false
    }

    return true
  }

  /**
   * @param {import("express").Request} request
   * @returns {Promise<boolean>}
  */
  async mutationDeleteTalents(args, request) {

    if(!request["isAuth"]){
      throw new Error("Authorization")      
    }

    const dataAuth = request["dataAuth"]["claim"]["data"]

    const controllerCompanyResponse = await this.#controllerCompany.deleteAllTalent(      
      dataAuth._id
    )
    if(controllerCompanyResponse.error != null){      
      return false
    }

    return true
  }

}

module.exports = HandlerCompany