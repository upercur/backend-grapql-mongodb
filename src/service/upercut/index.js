//@ts-check

const mongoose = require('mongoose');
const express = require("express");
const cors = require("cors");

const PkgJWT = require("../../pkg/pkg_jwt")

const ModelCompany = require("./models/company/index")
const ModelTalent = require("./models/talent/index")

const ControllerCompany = require("./controllers/company/index")
const ControllerTalent = require("./controllers/talent/index")

const HandlerCompany = require("./handlers/company/index");
const HandlerTalent = require("./handlers/talent/index");

const BuildGraphQLRouterCompany = require("./routers/graphql/company/index");
const BuildGraphQLRouterTalent = require("./routers/graphql/talent/index");

const GenerateAuthorization = require('./middleware/middleware_authorization');

const { 
  SERVICE_PORT, 
  SERVICE_NAME,
} = require("../../config/config_application");

( async ()=>{

  await mongoose.connect('mongodb://mongo:mongo@localhost:27017/?directConnection=true')
  const pkgJWT = new PkgJWT()

  const controllerCompany = new ControllerCompany(
    ModelCompany,
    ModelTalent,
    pkgJWT,
  )  

  const controllerTalent = new ControllerTalent(
    ModelCompany,
    ModelTalent,
    pkgJWT,
  )  

  const handlerCompany = new HandlerCompany(
    controllerCompany,
  )
  
  const handlerTalent = new HandlerTalent(
    controllerTalent,
  )
  
  const app = express()
  
  app.use(cors());
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json({ limit: "100mb" }))
  app.use(GenerateAuthorization(pkgJWT))
  
  app.use(
    "/api/v1/graphql/company",  
    BuildGraphQLRouterCompany(handlerCompany)
  )
  
  app.use(
    "/api/v1/graphql/talent",  
    BuildGraphQLRouterTalent(handlerTalent)
  )
  
  process.on('SIGINT', async () => {
    // await ConnectionMongoDB.close()
    console.log(`Service ${SERVICE_NAME} : ❌ Stop Running`)
    process.exit(0)
  });
  
  app.listen(SERVICE_PORT, () => {  
    // console.log(ConnectionMongoDB.readyState === 1 ? "Database: ✅ Succces Connected" : "Database: ❌ Something wrong with database")
    console.log(`Service ${SERVICE_NAME} : ✅ Running`)
  })

})()


