// @ts-check

const JWT = require("jsonwebtoken")

const { 
  TOKEN_SERVICE 
} = require("../config/config_application")

class PkgJWT{

  /** @constructor */
  constructor(){}

  /**
   * @typedef {object} VerificationResponse
   * @property {string} token
   * @property {any} claim
   */

  /** 
   * @param {string} token
   * @returns {{data:VerificationResponse, error:any}}
  */
  verification(token){

    /** @type {{data:VerificationResponse, error:any}} */
    let response = {
      data: {
        token: "",
        claim: null
      },
      error: null
    }

    try {      
      const decoded = JWT.verify(token, TOKEN_SERVICE)

      response.data.token = token
      response.data.claim = decoded
      
      return response      
    } catch (error) {
      response.error = error
      return response      
    }    

  }

  /**   
   * @param {{data:any, algorithm:string, expiresIn:number, tipe:string}} param0 
   * @returns {{token:string, error:any}}
  */
  generateToken({
    data,
    algorithm,
    expiresIn,
    tipe
  }){

    algorithm = algorithm || "HS512"
    data = (data||null)
    expiresIn = expiresIn || (60 * 60 * 24)

    console.log(data)

    /** @type {{token:string, error:any}} */
    let response = {
      token: "",
      error: null,      
    }

    try {      
      
      // @ts-ignore
      response.token = JWT.sign({data, tipe}, TOKEN_SERVICE, {
        expiresIn,
        algorithm
      })        

      return response
    } catch (error) {      
      response.error = error
      return response
    }

  }

}

module.exports = PkgJWT